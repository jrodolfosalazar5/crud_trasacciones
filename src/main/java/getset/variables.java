
package getset;

/**
 *
 * @author CEDEÑOS
 */
public class variables {
    private static String idhuerfano;
    private static String doctor;
    private static String nombre;
    private static String apellido;
    private static String cedula;
    private static String genero;
    private static String etnia;
    private static String fecha;
    
    public String getIdhuerfano() {
        return idhuerfano;
    }

    public void setIdhuerfano(String idhuerfano) {
        this.idhuerfano = idhuerfano;
    }
    
    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getEtnia() {
        return etnia;
    }

    public void setEtnia(String etnia) {
        this.etnia = etnia;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    
    private static String idtio;
    private static String idtipo;
    private static String nombretio;
    private static String cedulatio;
    private static String telefono;
    private static String direccion;
    
    public String getIdtio() {
        return idtio;
    }

    public void setIdtio(String idtio) {
        this.idtio = idtio;
    }

    public String getIdtipo() {
        return idtipo;
    }

    public void setIdtipo(String idtipo) {
        this.idtipo = idtipo;
    }

    public String getNombretio() {
        return nombretio;
    }

    public void setNombretio(String nombretio) {
        this.nombretio = nombretio;
    }

    public String getCedulatio() {
        return cedulatio;
    }

    public void setCedulatio(String cedulatio) {
        this.cedulatio = cedulatio;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    

}
