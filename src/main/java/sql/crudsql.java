
package sql;

import getset.variables;
import javax.swing.JOptionPane;
import java.sql.Connection;
import java.sql.ResultSet;

public class crudsql extends conexionsql{
    java.sql.Statement st;
    ResultSet rs;
    variables var = new variables();
    
    public void insertar(String doctor, String nombre, String apellido, String cedula, String genero, String etnia, String fecha) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "insert into HUERFANO(id_doctor,nombre_huerfano,apellido_huerfano,cedula_huerfano,genero,etnia,fecha_nacimiento) "
                    + "values('" + doctor + "','" + nombre + "','" + apellido + "','" + cedula + "','" + genero + "','" + etnia + "','" + fecha + "');";
            st.execute(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "Transaccion ejecutada correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El registro no se guardo " + e, "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
   }
    public void consulta(String id_huerfano) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "select * from huerfano where id_huerfano='" + id_huerfano + "';";
            rs = st.executeQuery(sql);
            if (rs.next()) {
                var.setDoctor(rs.getString("id_doctor"));
                var.setNombre(rs.getString("nombre_huerfano"));
                var.setApellido(rs.getString("apellido_huerfano"));
                var.setCedula(rs.getString("cedula_huerfano"));
                var.setGenero(rs.getString("genero"));
                var.setEtnia(rs.getString("etnia"));
                var.setFecha(rs.getString("fecha_nacimiento"));
            } else {
                var.setDoctor("");
                var.setNombre("");
                var.setApellido("");
                var.setCedula("");
                var.setGenero("");
                var.setEtnia("");
                var.setFecha("");
                JOptionPane.showMessageDialog(null, "no se encontro registro", "sin registro", JOptionPane.INFORMATION_MESSAGE);
            }
            st.close();
            conexion.close();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "error en programa " + e, "Erro de sistema", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void modificar(String doctor, String nombre, String apellido, String cedula, String genero, String etnia, String fecha, String id_huerfano) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "update huerfano set id_doctor='" + doctor + "',nombre_huerfano='" + nombre + "',apellido_huerfano='" + apellido + 
                    "',cedula_huerfano='" + cedula + "',genero='" + genero + "',etnia='" + etnia + "',fecha_nacimiento='" + fecha + "'"
                    + " where id_huerfano='" + id_huerfano + "'; ";
            st.executeUpdate(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "El registro se actualizo", "Exito", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al actualizar " + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    
    
    
    
    public void insertar2(String tipo, String nombre, String cedula, String telefono, String direccion) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "insert into tio(id_tipotio,nombres_tio,cedula_tio,telefono_tio,direccion) "
                    + "values('" + tipo + "','" + nombre + "','" + cedula + "','" + telefono + "','" + direccion + "');";
            st.execute(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "El registro se guardo correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El registro no se guardo " + e, "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
   }
    
     public void consulta2(String id_tio) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "select * from tio where id_tio='" + id_tio + "';";
            rs = st.executeQuery(sql);
            if (rs.next()) {
                var.setIdtipo(rs.getString("id_tipotio"));
                var.setNombretio(rs.getString("nombres_tio"));
                var.setCedulatio(rs.getString("cedula_tio"));
                var.setTelefono(rs.getString("telefono_tio"));
                var.setDireccion(rs.getString("direccion"));
            } else {
                var.setIdtio("");
                var.setNombretio("");
                var.setCedulatio("");
                var.setTelefono("");
                var.setDireccion("");
                JOptionPane.showMessageDialog(null, "no se encontro registro", "sin registro", JOptionPane.INFORMATION_MESSAGE);
            }
            st.close();
            conexion.close();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "error en programa " + e, "Erro de sistema", JOptionPane.ERROR_MESSAGE);
        }
    }
     
     public void modificar2(String tipo, String nombre, String cedula, String telefono, String direccion, String id_tio) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "update tio set id_tio='" + tipo + "',nombres_tio='" + nombre + "',cedula_tio='" + cedula + 
                    "',telefono_tio='" + telefono + "',direccion='" + direccion + "'" + " where id_tio='" + id_tio + "'; ";
            st.executeUpdate(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "El registro se actualizo", "Exito", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al actualizar " + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
}
